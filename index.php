<!DOCTYPE html>
<html class="no-js" lang="en">
    <?php include('includes/html_head.html'); ?>

    <body>
        <div class="off-canvas-wrap" data-offcanvas>    
            <div class="inner-wrap">
                <?php include('includes/nav_offcanvas.html'); ?>
                <?php include 'includes/mast_head.html'; ?>
                
                <header>
                    <div class=""><!-- If you want a full-width bg colour, add the class here -->
                        <div class="row">
                            <div class="small-12 column clearfix">
                                
                            </div><!-- column -->
                        </div><!-- row -->
                    </div>
                    <div class=""><!-- If you want a full-width bg colour, add the class here -->
                        <div class="row">
                            <div class="small-12 column clearfix">
                                <?php include('includes/nav_main.html'); ?>
                            </div><!-- column -->
                        </div><!-- row -->
                    </div>
                </header>
    
                <div class="row">
                    <div class="small-12 column">
                        <img src="http://placehold.it/940x250" style="width:100%; margin-bottom: 1em; margin-top: 1em" />
                    </div><!-- column -->
                </div> <!-- row -->
            
                <div class="row">
                    <div class="small-12 medium-4 column clearfix">
                        <div class="panel">
                            <h2>Title</h2>
                            <p>content to go here</p>
                        </div>
                    </div><!-- column -->
            
                    <div class="small-12 medium-4 column clearfix">
                        <div class="panel">
                            <h2>Title</h2>
                            <p>content to go here</p>
                        </div>
                    </div><!-- column -->
            
                    <div class="small-12 medium-4 column clearfix">
                        <div class="panel">
                            <h2>Title</h2>
                            <p>content to go here</p>
                        </div>
                    </div><!-- column -->
                </div><!-- row -->
    
                <?php include('includes/page_footer.html'); ?>
            </div><!-- inner-wrap -->
        </div> <!-- off-canvas-wrap -->
            
        <?php include('includes/html_footer.html'); ?>
    </body>

</html>