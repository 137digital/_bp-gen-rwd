<!DOCTYPE html>
<html class="no-js" lang="en">
    <?php include('/includes/html_head.html'); ?>

    <body>
        <div class="off-canvas-wrap" data-offcanvas>
            <div class="inner-wrap">
                <?php include('/includes/nav_offcanvas.html'); ?>
                <?php include '/includes/mast_head.html'; ?>
            
                <header>
                    <div class=""><!-- If you want a full-width bg colour, add the class here -->
                        <div class="row">
                            <div class="small-12 column clearfix">
                                
                            </div><!-- column -->
                        </div><!-- row -->
                    </div>
                    <div class=""><!-- If you want a full-width bg colour, add the class here -->
                        <div class="row">
                            <div class="small-12 column clearfix">
                                <?php include('/includes/nav_main.html'); ?>
                            </div><!-- column -->
                        </div><!-- row -->
                    </div>
                </header>
    
                <div class="row">
                    <div class="small-12 column clearfix">
                        <h1>Page Not Found</h1>
                        <p>Sorry, but the page could not be found. Please try again or return to the <a title="Homepage" href="/">Homepage</a>.</p>
                    </div> <!-- column -->
                </div> <!-- row -->
    
                <?php include('/includes/page_footer.html'); ?>
            </div><!-- inner-wrap -->
        </div> <!-- off-canvas-wrap -->
            
        <?php include('/includes/html_footer.html'); ?>
    </body>
</html>
