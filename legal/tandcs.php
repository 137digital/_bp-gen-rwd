<!DOCTYPE html>
    <?php include('./includes/html_head.html'); ?>

    <body>
        <div class="off-canvas-wrap" data-offcanvas>   
            <div class="inner-wrap">
                <?php include('../includes/nav_offcanvas.html'); ?>
                <?php include '../includes/mast_head.html'; ?>
    
                <header>
                    <div class=""><!-- If you want a full-width bg colour, add the class here -->
                        <div class="row">
                            <div class="small-12 column clearfix">
                                
                            </div><!-- column -->
                        </div><!-- row -->
                    </div>
                    <div class=""><!-- If you want a full-width bg colour, add the class here -->
                        <div class="row">
                            <div class="small-12 column clearfix">
                                <?php include('../includes/nav_main.html'); ?>
                            </div><!-- column -->
                        </div><!-- row -->
                    </div>
                </header>
    
                <div class="row">
                    <div class="small-12 medium-9 column">
                        <p>CONTENT</p>
                    </div><!-- column -->
    
                    <div class="small-12 medium-3 column">
                        <p>SIDEBAR</p>
                    </div> <!-- column -->      
                </div> <!-- end -->
    
                <?php include('../includes/page_footer.html'); ?>
            </div><!-- inner-wrap -->
        </div> <!-- off-canvas-wrap -->
        <?php include('../includes/html_footer.html'); ?>
    </body>
</html>
