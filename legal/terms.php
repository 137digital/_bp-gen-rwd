<!DOCTYPE html>
    <?php include('/includes/html_head.html'); ?>

    <body>
        <div class="off-canvas-wrap" data-offcanvas>   
            <div class="inner-wrap">
                <?php include('/includes/nav_offcanvas.html'); ?>
                <?php include '/includes/mast_head.html'; ?>
    
                <header>
                    <div class=""><!-- If you want a full-width bg colour, add the class here -->
                        <div class="row">
                            <div class="small-12 column clearfix">
                                
                            </div><!-- column -->
                        </div><!-- row -->
                    </div>
                    <div class=""><!-- If you want a full-width bg colour, add the class here -->
                        <div class="row">
                            <div class="small-12 column clearfix">
                                <?php include('/includes/nav_main.html'); ?>
                            </div><!-- column -->
                        </div><!-- row -->
                    </div>
                </header>
    
                <div class="row">
                    <div class="small-12 medium-9 column">
                        <h1 class="pageSection__title">Terms of use</h1>
                        <p>Welcome to the Black Buzzard website. If you continue to browse and use this website, you are agreeing to comply with and be bound by the following terms and conditions of use which, together with our privacy policy, govern Black Buzzard's relationship with you in relation to this website. If you disagree with any part of these terms and conditions, please do not use our website.</p>
                        <p>The term "Black Buzzard" refers to the owner of the website. The terms "you" and "your"&nbsp;refer to the user or viewer of our website.</p>
                        <p>The use of this website is subject to the following terms of use:</p>
                        <p>The content of the pages of this website is for your general information and use only. It is subject to change without notice.</p>
                        <p>Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.</p>
                        <p>Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.</p>
                        <p>This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.</p>
                        <p>All trademarks reproduced in this website, which are not the property of, or licensed to the operator, are acknowledged on the website.</p>
                        <p>Unauthorised use of this website may give rise to a claim for damages and/or be a criminal offence.</p>
                        <p>From time to time, this website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s).</p>
                        <p>We have no responsibility for the content of the linked website(s). Your use of this website and any dispute arising out of such use of the website is subject to the laws of England, Northern Ireland, Scotland and Wales.</p>
                    </div><!-- column -->
    
                    <div class="small-12 medium-3 column">
                        <p>SIDEBAR</p>
                    </div> <!-- column -->      
                </div> <!-- end -->
    
                <?php include('/includes/page_footer.html'); ?>
            </div><!-- inner-wrap -->
        </div> <!-- off-canvas-wrap -->
        <?php include('/includes/html_footer.html'); ?>
    </body>
</html>
