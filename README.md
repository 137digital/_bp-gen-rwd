# README #

## Domain ##
http://www.domain.co.uk

## Commit ID - from which this project was cloned ##
e.g. ea616c6

## Name of hosting provider ##
HOSTING PROVIDER

## Environments ##
Dev - dev.domain.co.uk
Staging - staging.domain.co.uk
Production - www.domain.co.uk

## Setup ##
Set Beanstalk deploy to ignore these files so they're not uploaded to the site:

  * README.md
  * nbproject

## Notes ##


